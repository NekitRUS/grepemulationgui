﻿using System;
using System.Windows.Forms;
using System.Text;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form2: Form
    {
        public Form2(string path)
        {
            InitializeComponent();
            textBoxTitle.Text = path;
            richTextBoxContent.Text = File.ReadAllText(path, Encoding.GetEncoding(1251));
        }
    }
}
