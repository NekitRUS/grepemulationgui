﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1: Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileOpenWindow = new OpenFileDialog();
            fileOpenWindow.Filter = "Текстовые файлы (*.txt)|*.txt";
            if (fileOpenWindow.ShowDialog() == DialogResult.OK)
            {
                textBoxPath.Text = fileOpenWindow.FileName;
                buttonShowFile.Enabled = true;
            }

        }

        private void buttonShowFile_Click(object sender, EventArgs e)
        {
            FormCollection forms = Application.OpenForms;
            int index = 0;
            foreach (Form f in forms)
            {
                if (f.GetType().Name == "Form2")
                {
                    forms[index].Close();
                    break;
                }
                index++;
            }
            Form2 FileContent = new Form2(textBoxPath.Text);
            FileContent.Show();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if ((textBoxPath.Text == "") || (textBoxPattern.Text == ""))
            {
                MessageBox.Show("Выберите файл или задайте шаблон для поиска!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    textBoxResult.Clear();
                    Regex pattern;
                    if (checkBoxI.Checked)
                    {
                        pattern = new Regex(textBoxPattern.Text, RegexOptions.IgnoreCase);
                    }
                    else
                    {
                        pattern = new Regex(textBoxPattern.Text);
                    }
                    StreamReader sr = new StreamReader(textBoxPath.Text, Encoding.GetEncoding(1251));
                    string line;
                    int lineCounter = 1;
                    int matchCounter = 0;
                    do
                    {
                        line = sr.ReadLine();
                        MatchCollection matches = pattern.Matches(line);
                        if (!checkBoxС.Checked)
                        {
                            if (checkBoxN.Checked)
                            {
                                foreach (Match match in matches)
                                {
                                    string result = "В строке №" + lineCounter.ToString() + " найдено соответствие: " + match.Value;
                                    textBoxResult.AppendText(result + Environment.NewLine + "Оригинальная строка: " + line + Environment.NewLine + Environment.NewLine);
                                }
                            }
                            else
                            {
                                foreach (Match match in matches)
                                {
                                    string result = "Найдено соответствие: " + match.Value;
                                    textBoxResult.AppendText(result + Environment.NewLine + "В строке: " + line + Environment.NewLine + Environment.NewLine);
                                }
                            }
                        }
                        lineCounter++;
                        matchCounter += matches.Count;
                    }
                    while (!sr.EndOfStream);
                    sr.Close();
                    textBoxResult.AppendText("Всего найдено " + matchCounter + " соответсвий.");
                }
                catch (ArgumentException)
                {
                    MessageBox.Show("Недопустимое регулярное выражение!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка при открытии файла!" + ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

    }
}
